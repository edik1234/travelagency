const db = require('../config/dbconnect')

exports.one = function(sql) {
    return new Promise((resolve) => {
        let query = db.query(sql, (error, results) => {
            if (error) {
                console.error('Read problem: ' + error)
            }
            resolve(results)
        })
    })
    .catch((e) => {
        console.error('Have problem3: ', e)
    })
}

exports.two = function(sql, post) {
    return new Promise((resolve) => {
        let query = db.query(sql, post, (error, results) => {
            if (error) {
                console.error('Read problem: ' + error)
            }
            resolve(results)
        })
    })
    .catch((e) => {
        console.error('Have problem3: ', e)
    })
}