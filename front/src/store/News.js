import newsApi from '../../api/news'

export default {
    state: {
        news: []
    },
    getters: {
        AllNews(state) {
            return state.news
        }
    },
    action: {
        GetNews(ctx) {
            newsApi.getNewsList()
                .then((response) => {
                    ctx.commit('UpdateNews', response.data)
                })
        }
    },
    mutations: {
        UpdateNews(state, news) {
            state.news = news
        }
    }
}